package com.example.home.garbage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class TypesActivity extends AppCompatActivity {

    private String garbageType;
    private TextView[] headerRow;
    private TableLayout tableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_types);

        Intent intent = getIntent();
        garbageType = intent.getStringExtra("GarbageType");

        TextView type = (TextView) findViewById(R.id.TypeNameTextView);
        type.setText(garbageType);

        headerRow = new TextView[2];
        tableLayout = (TableLayout) findViewById(R.id.TypesTableLayout);

        headerRow[0] = (TextView) findViewById(R.id.HeaderTypeTextView1);
        headerRow[1] = (TextView) findViewById(R.id.HeaderTypeTextView2);

        fillTable();
    }

    private void fillTable(){
        InputStream myInput = getResources().openRawResource(R.raw.garbagefile);

        POIFSFileSystem myFileSystem = null;
        HSSFWorkbook myWorkBook = null;

        try {
            myFileSystem = new POIFSFileSystem(myInput);
            myWorkBook = new HSSFWorkbook(myFileSystem);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }

        HSSFSheet mySheet = myWorkBook.getSheetAt(0);
        Iterator rowIter = mySheet.rowIterator();

        HSSFRow myRow = (HSSFRow) rowIter.next();

        headerRow[0].setText(myRow.getCell(1).toString());
        headerRow[1].setText(myRow.getCell(2).toString());

        while (rowIter.hasNext()){
            myRow = (HSSFRow) rowIter.next();
            String cell = myRow.getCell(0).toString();
            if (cell.equalsIgnoreCase(garbageType)){
                fillRow(myRow);
                continue;
            }
            String[] cell4 = myRow.getCell(3).toString().split(",");
            for (int i=0; i<cell4.length; i++){
                cell4[i] = cell4[i].trim();
                if (cell4[i].equalsIgnoreCase(garbageType)){
                    fillRow(myRow);
                    break;
                }
            }
        }

    }

    private void fillRow(HSSFRow myRow){
        TableRow tableRow = new TableRow(this);
        tableRow.setWeightSum(2);
        TextView text1 = new TextView(this);
        TextView text2 = new TextView(this);
        TableRow.LayoutParams loparams = new TableRow.LayoutParams();
        loparams.weight = 1;
        loparams.width = 0;
        text1.setLayoutParams(loparams);
        text2.setLayoutParams(loparams);
        text1.setTextSize(20);
        text2.setTextSize(20);
        if (myRow.getCell(1) == null){
            text1.setText("");
        } else {
            text1.setText(myRow.getCell(1).toString());
        }
        if (myRow.getCell(2) == null){
            text2.setText("");
        } else {
            text2.setText(myRow.getCell(2).toString());
        }
        tableRow.addView(text1);
        tableRow.addView(text2);
        tableLayout.addView(tableRow);
    }
}
