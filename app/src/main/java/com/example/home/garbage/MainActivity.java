package com.example.home.garbage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String[] CITIES;
    private int SELECTED_CITY;
    private HSSFWorkbook myWorkBook = null;
    private Button select_city_button;
    private Spinner city_spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setCITY();
        city_spinner = (Spinner) findViewById(R.id.city_spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, CITIES);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        city_spinner.setAdapter(adapter);

        select_city_button = (Button) findViewById(R.id.city_button);
        select_city_button.setOnClickListener(this);

    }

    private void setCITY(){
        InputStream myInput = getResources().openRawResource(R.raw.garbagefile);

        POIFSFileSystem myFileSystem = null;

        try {
            myFileSystem = new POIFSFileSystem(myInput);
            myWorkBook = new HSSFWorkbook(myFileSystem);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }

        HSSFSheet mySheet = myWorkBook.getSheetAt(1);

        Iterator rowIter = mySheet.rowIterator();
        rowIter.next();

        ArrayList<String> citiesList = new ArrayList<>();
        while(rowIter.hasNext()) {
            HSSFRow myRow = (HSSFRow) rowIter.next();
            HSSFCell myCell = myRow.getCell(0);
            citiesList.add(myCell.toString());
        }

        CITIES = new String[citiesList.size()];
        for (int i=0; i<CITIES.length; i++){
            CITIES[i] = citiesList.get(i);
        }
    }

    @Override
    public void onClick(View v) {
        if (city_spinner.getSelectedItem() == null) return;
        SELECTED_CITY = (int) city_spinner.getSelectedItemId()+1;
        sendMessage();
    }

    public void sendMessage() {
        Intent intent = new Intent(this, InfoActivity.class);
        intent.putExtra("CityID", SELECTED_CITY);
        intent.putExtra("CityName", CITIES[SELECTED_CITY-1]);
        startActivity(intent);
    }
}
