package com.example.home.garbage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class ScheduleActivity extends AppCompatActivity {

    private int SELECTED_CITY;
    private TextView[] headerRow;
    private TextView[] daysRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        Intent intent = getIntent();
        SELECTED_CITY = intent.getIntExtra("CityID", 1);

        TextView text = (TextView) findViewById(R.id.CityScheduleTextView);
        text.setText(intent.getStringExtra("CityName"));

        headerRow = new TextView[3];
        daysRow = new TextView[3];

        headerRow[0] = (TextView) findViewById(R.id.HeaderTextView1);
        headerRow[1] = (TextView) findViewById(R.id.HeaderTextView2);
        headerRow[2] = (TextView) findViewById(R.id.HeaderTextView3);

        daysRow[0] = (TextView) findViewById(R.id.DayTextView1);
        daysRow[1] = (TextView) findViewById(R.id.DayTextView2);
        daysRow[2] = (TextView) findViewById(R.id.DayTextView3);

        fillTable();
    }

    private void fillTable(){
        InputStream myInput = getResources().openRawResource(R.raw.garbagefile);

        POIFSFileSystem myFileSystem = null;
        HSSFWorkbook myWorkBook = null;

        try {
            myFileSystem = new POIFSFileSystem(myInput);
            myWorkBook = new HSSFWorkbook(myFileSystem);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }

        HSSFSheet mySheet = myWorkBook.getSheetAt(1);
        HSSFRow myRow = mySheet.getRow(0);

        Iterator cellIter = myRow.cellIterator();
        cellIter.next();
        int i = 0;
        while(cellIter.hasNext()){
            HSSFCell myCell = (HSSFCell) cellIter.next();
            headerRow[i++].setText(myCell.toString());
        }

        myRow = mySheet.getRow(SELECTED_CITY);
        cellIter = myRow.cellIterator();
        cellIter.next();
        i=0;
        while(cellIter.hasNext()){
            HSSFCell myCell = (HSSFCell) cellIter.next();
            daysRow[i++].setText(myCell.toString());
        }
    }
}
