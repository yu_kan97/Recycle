package com.example.home.garbage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InfoActivity extends AppCompatActivity implements View.OnClickListener{

    private Button scheduleButton;
    private Button typesButton;
    private int SELECTED_CITY;
    private String CITY_NAME;
    private EditText garbageTypeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        TextView text = (TextView) findViewById(R.id.CityTextView);
        Intent intent = getIntent();
        CITY_NAME = intent.getStringExtra("CityName");
        text.setText(CITY_NAME);

        garbageTypeEditText = (EditText) findViewById(R.id.GarbageTypeEditText);

        SELECTED_CITY = intent.getIntExtra("CityID", 0);
        scheduleButton = (Button) findViewById(R.id.ScheduleButton);
        typesButton = (Button) findViewById(R.id.GarbageTypeButton);
        scheduleButton.setOnClickListener(this);
        typesButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ScheduleButton:
                Intent scheduleIntent = new Intent(this, ScheduleActivity.class);
                scheduleIntent.putExtra("CityID", SELECTED_CITY);
                scheduleIntent.putExtra("CityName", CITY_NAME);
                startActivity(scheduleIntent);
                break;

            case R.id.GarbageTypeButton:
                Intent typesIntent = new Intent(this, TypesActivity.class);
                String garbageType = garbageTypeEditText.getText().toString();
                if (garbageType.length() < 2){
                    Toast.makeText(this, "Fill the field", Toast.LENGTH_SHORT).show();
                    break;
                }
                typesIntent.putExtra("GarbageType", garbageType);
                startActivity(typesIntent);
                break;

            default:
                break;
        }
    }
}
